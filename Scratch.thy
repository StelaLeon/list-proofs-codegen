theory Scratch
  imports Main
begin


fun remove:: "'a => 'a list => 'a list"
  where 
     "remove x [] = []" | 
     "remove x(y#ys) = (if x=y then ys else y#(remove x ys)) "


fun length :: "'a list \<Rightarrow> nat"
  where
    "length [] = 0" |
    "length (x#xs) = 1 + length xs"


fun foldr :: "('a \<Rightarrow> 'b \<Rightarrow> 'b) \<Rightarrow> 'b \<Rightarrow> 'a list \<Rightarrow> 'b"
  where
    "foldr f z [] = z" |
    "foldr f z (x#xs) = f x (foldr f z xs)"


fun const :: "'a \<Rightarrow> 'b \<Rightarrow> 'a"
  where
    "const a b = a"



lemma "length xs = foldr (const (\<lambda>x . x + 1)) 0 xs"
  apply(induction xs)
   apply(auto)
  done
(*by(induction xs) auto *)

(*this is a comment*)

fun splice :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"
  where
    "splice [] [] = []" | 
    "splice [] ys = ys" |
    "splice xs [] = xs" |
    "splice (x#xs) (y#ys) = x # y # splice xs ys"

fun sum :: "int list \<Rightarrow> int" 
  where 
    "sum [] = 0" | 
    "sum (x#xs) = x + sum xs"

lemma "sum (splice xs ys) = sum xs + sum ys"
  by(induction xs ys rule:splice.induct) auto


fun rev :: "'a list \<Rightarrow> 'a list" 
  where 
    "rev [] = []" | 
    "rev (x#xs) = (rev xs) @ [x]"

fun rev_acc :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"
  where
    "rev_acc [] acc = acc" |
    "rev_acc (x#xs) acc = rev_acc xs (x # acc)"

(*rev_acc xs [] @ [a] = rev_acc xs [a] *)


lemma rev_acc_gen: "rev_acc xs acc = (rev_acc xs []) @ acc"
proof(induction xs arbitrary:acc)
  case Nil
  then show ?case by simp
next
  case (Cons a xs)
  have "rev_acc (a # xs) acc = rev_acc xs (a # acc)" by simp
  also have "... = rev_acc xs [] @ (a # acc)" using Cons.IH by blast 
  also have "... = (rev_acc xs [] @ [a]) @ acc" by auto 
  also have "... = (rev_acc xs [a]) @ acc" using Cons.IH by metis 
  also have "... = (rev_acc (a#xs) []) @ acc" by auto
  finally have "rev_acc (a#xs)acc = (rev_acc (a#xs) []) @ acc" by blast
  then show ?case by blast
qed


lemma "rev xs = rev_acc xs []" 
proof (induction xs)
case Nil
  then show ?case by simp
next
  case (Cons a xs)
  have "rev (a # xs) = (rev xs) @ [a]" by simp
  also have "... = (rev_acc xs []) @ [a]" using Cons.IH try0 by simp
  also have "... = rev_acc xs [a]" using rev_acc_gen by metis
  also have "... = rev_acc (a#xs) []" by simp
  finally have "rev (a # xs) = rev_acc (a#xs) []" by blast
  then show ?case by simp
qed

value "remove (2::int) [1,2,3]"
value "length [1,2,0,0,0,0,2,3::int]"

end